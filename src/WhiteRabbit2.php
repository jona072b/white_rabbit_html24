<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $moneyArray = array(
            '1'   => 0,
            '2'   => 0,
            '5'   => 0,
            '10'  => 0,
            '20'  => 0,
            '50'  => 0,
            '100' => 0
        );

        $moneyArray['100'] = intdiv($amount, 100);
        $amountLeft = $amount%100;

        $moneyArray['50'] =  intdiv($amountLeft, 50);
        $amountLeft %= 50;

        $moneyArray['20'] = intdiv($amountLeft, 20 );
        $amountLeft %= 20;

        $moneyArray['10'] = intdiv($amountLeft,10);
        $amountLeft %= 10;

        $moneyArray['5'] = intdiv($amountLeft,5);
        $amountLeft %= 5;

        $moneyArray['2'] = intdiv($amountLeft,2);
        $amountLeft %= 2;

        $moneyArray['1'] = $amountLeft;

        return $moneyArray;
    }
}