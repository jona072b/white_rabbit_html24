<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $string = file_get_contents($filePath);
        $string = strtolower($string);
        $string = preg_replace("/[^a-z]+/", "", $string);
        $array = str_split($string,1);

        return $array;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //Counts the number of occurences in the parsed array.
        $countedArray = array_count_values($parsedFile);

        //sorts the occurences low to high
        asort($countedArray);

        $numberedArray = $countedArray;
        sort($numberedArray);

        $count = count($countedArray)-1;

        $middleValue = round($count/2)-1;


        echo "Middlevalue: $middleValue\n";

        $occurrences = $numberedArray[$middleValue];


        $letter = array_search($occurrences, $countedArray);


        return $letter;

        /*  Not able to figure out why test number 3 and 5 are failing.
         *  In test 3 the middlevalue would have to be 11 and not 12
         *  but that would make test 1,2 and 4 to fail.
         *
         *  In test 5, the middlevalue would have to be 2, but that just seems wrong.
         */


    }
}